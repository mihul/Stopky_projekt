/**
  ******************************************************************************
  * @file    GPIO_Toggle\main.c
  * @author  MCD Application Team
  * @version V2.0.4
  * @date    26-April-2018
  * @brief   This file contains the main function for GPIO Toggle example.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2014 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm8s.h"

//#include "stm8s_it.h"    /* SDCC patch: required by SDCC for interrupts */
#include <stdint.h>
/*---------------------------------------------------------------*/
#define TRANZISTOR_MIN      GPIOC, GPIO_PIN_1
#define TRANZISTOR_DESSEC   GPIOC, GPIO_PIN_2
#define TRANZISTOR_SEC      GPIOC, GPIO_PIN_3
#define TRANZISTOR_STMS     GPIOC, GPIO_PIN_4 
#define LED_TECKA           GPIOF, GPIO_PIN_6
#define RESET               GPIOB, GPIO_PIN_3
#define STARTSTOP           GPIOB, GPIO_PIN_2
//deklarace proměnných
uint8_t milisekundy = 0;
uint8_t stomilisekund = 0;
uint8_t sekundy = 0;
uint8_t desitkysekund = 0;
uint8_t minuty = 0;
uint8_t tranzistor = 0;
uint8_t castranzistoru = 0;
uint8_t jednasekunda = 0;
uint8_t tlacitko_puvodni = 0;
uint8_t stav = 0;
uint8_t znaky[10]={ 0b01110111,     //0
                    0b01000001,     //1
                    0b00111011,     //2
                    0b01101011,     //3
                    0b01001101,     //4
                    0b01101110,     //5
                    0b01111110,     //6
                    0b01000011,     //7
                    0b01111111,     //8
                    0b01101111};    //9

// rutina obsluhy přerušení
INTERRUPT_HANDLER(TIM2_UPD_OVF_BRK_IRQHandler, 13)
//void preruseni (void)
{
    uint8_t tmp = 0;
    uint8_t i = 0;
    // negace výstupu
    GPIO_WriteReverse(GPIOC, GPIO_PIN_5);
    // vyčištění vlajky žádosti přeručení
    TIM2_ClearITPendingBit(TIM2_IT_UPDATE);

    //počítání času
    if (stav == 1)
    {
      milisekundy++;
    }
    if (milisekundy > 99)
    {
        milisekundy = 0;
        stomilisekund++;

        if (stomilisekund > 9)
        { 
            if(jednasekunda ==0)
            {
              jednasekunda = 1;
            }
            else
            {
              jednasekunda =0;
            }
            stomilisekund = 0;
            sekundy++;

            if (sekundy > 9)
            {
                sekundy = 0;
                desitkysekund++;

                if (desitkysekund > 5)
                {
                    desitkysekund = 0;
                    minuty++;

                    if (minuty > 9)
                    {
                        minuty = 0;
                    }
                }
            }
        }
    }

    //ovládání tranzistorů
    castranzistoru++;
    if (castranzistoru == 3)
    {
        castranzistoru = 0;
        tranzistor++;
        
        if (tranzistor >3)
        {
            tranzistor = 0;
        }
    }

    if (tranzistor == 0)
    {
      //zapnout tranzistor stms
      GPIO_WriteHigh(TRANZISTOR_STMS);
      GPIO_WriteLow(TRANZISTOR_SEC);
      GPIO_WriteLow(TRANZISTOR_DESSEC);
      GPIO_WriteLow(TRANZISTOR_MIN);

      i = stomilisekund;
      tmp = znaky[i] & 0xf0;
      tmp = tmp >>1;
      GPIO_Write(GPIOF,tmp);

      tmp = znaky[i] & 0x0f;
      tmp = tmp <<4;
      GPIO_Write(GPIOG,tmp);
      
    }
    else if (tranzistor == 1)
    {
      //zapnout tranzistor sec
      GPIO_WriteLow(TRANZISTOR_STMS);
      GPIO_WriteHigh(TRANZISTOR_SEC);
      GPIO_WriteLow(TRANZISTOR_DESSEC);
      GPIO_WriteLow(TRANZISTOR_MIN);

      i = sekundy;
      tmp = znaky[i] & 0xf0;
      tmp = tmp >>1;
      GPIO_Write(GPIOF,tmp);

      tmp = znaky[i] & 0x0f;
      tmp = tmp <<4;
      GPIO_Write(GPIOG,tmp);

      if (jednasekunda == 1)
      {
        GPIO_WriteHigh(LED_TECKA);
      }
      else
      {
        GPIO_WriteLow(LED_TECKA);
      }
    }
    else if (tranzistor == 2)
    {
      //zapnout tranzistor des sec
      GPIO_WriteLow(TRANZISTOR_STMS);
      GPIO_WriteLow(TRANZISTOR_SEC);
      GPIO_WriteHigh(TRANZISTOR_DESSEC);
      GPIO_WriteLow(TRANZISTOR_MIN);

      i = desitkysekund;
      tmp = znaky[i] & 0xf0;
      tmp = tmp >>1;
      GPIO_Write(GPIOF,tmp);

      tmp = znaky[i] & 0x0f;
      tmp = tmp <<4;
      GPIO_Write(GPIOG,tmp);
    }
    else 
    {
      //zapnout tranzistor min
      GPIO_WriteLow(TRANZISTOR_STMS);
      GPIO_WriteLow(TRANZISTOR_SEC);
      GPIO_WriteLow(TRANZISTOR_DESSEC);
      GPIO_WriteHigh(TRANZISTOR_MIN); 

      i = minuty;
      tmp = znaky[i] & 0xf0;
      tmp = tmp >>1;
      GPIO_Write(GPIOF,tmp);

      tmp = znaky[i] & 0x0f;
      tmp = tmp <<4;
      GPIO_Write(GPIOG,tmp); 
    }
}   

// hlavní funkce
void main(void)
{
    // nastavení pracovní frekvance na 16 MHz
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1);
    // inicializace GPIO
    GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_FAST); //testovací LED
    GPIO_Init(GPIOC, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_FAST); // tranzistor st ms
    GPIO_Init(GPIOC, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_FAST); // tranzistor sec
    GPIO_Init(GPIOC, GPIO_PIN_2, GPIO_MODE_OUT_PP_LOW_FAST); // tranzistor des sec
    GPIO_Init(GPIOC, GPIO_PIN_1, GPIO_MODE_OUT_PP_LOW_FAST); // tranzistor min

    GPIO_Init(GPIOF, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_FAST); //LED tečka
    GPIO_Init(GPIOF, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_FAST); //LED C
    GPIO_Init(GPIOF, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_FAST); //LED D
    GPIO_Init(GPIOF, GPIO_PIN_3, GPIO_MODE_OUT_PP_LOW_FAST); //LED E
    GPIO_Init(GPIOG, GPIO_PIN_7, GPIO_MODE_OUT_PP_LOW_FAST); //LED G
    GPIO_Init(GPIOG, GPIO_PIN_6, GPIO_MODE_OUT_PP_LOW_FAST); //LED F
    GPIO_Init(GPIOG, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_FAST); //LED A
    GPIO_Init(GPIOG, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_FAST); //LED B

    GPIO_Init(GPIOB, GPIO_PIN_2, GPIO_MODE_IN_FL_NO_IT); //tlačítko STOP/START
    GPIO_Init(GPIOB, GPIO_PIN_3, GPIO_MODE_IN_FL_NO_IT); //tlačítko RESET


    // inicializaci čítače
    TIM2_TimeBaseInit(TIM2_PRESCALER_1, 16086); //spočítaný 16666
    // povolení přeřušení při přetečení
    TIM2_ITConfig(TIM2_IT_UPDATE, ENABLE);
    // spuštění čítače
    TIM2_Cmd(ENABLE);
    // nastavení priority přerušení
    ITC_SetSoftwarePriority(ITC_IRQ_TIM2_OVF, ITC_PRIORITYLEVEL_1);
    // globální povolení přeřušení MCU
    enableInterrupts();
    // zacyklení programu
    while (1)

    { //reset
      if (GPIO_ReadInputPin(RESET))
      {
        milisekundy = 0;
        stomilisekund = 0;
        sekundy = 0;
        desitkysekund = 0;
        minuty = 0;
        stav = 0;
        jednasekunda = 0;
      }
      else
      {
        ;
      }
      //start stop
      if (GPIO_ReadInputPin(STARTSTOP))
      {
        if (tlacitko_puvodni == 0)
        {
          tlacitko_puvodni = 1;
          if (stav == 1)
          {
            stav = 0;
          }
          else
          {
            stav = 1;
          }
        }   
      }
      else
        {
          tlacitko_puvodni = 0;
        }
    }    
}


#ifdef USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif