# Stopky


## Základní informace

V mém projektu jsem vytvořil stopky, které jsou schopny počítat do 9 minut, 59 sekund a 9 desetin.
To zajišťují 4 rozdílné sedmisegmenty.
Při stisknutí tlačítka START/STOP při výchozím spustění se nám spustí časování, které postupně rozsvicuje sedmisegmenty podle uběhlého času.
Spouštění a zastavování je ovládáno pomocí tlačítka.
Stejě tak nulování, které nám umožňuje kompletní vynulovaní průběhu časovaní a možnost začít časovat znovu. 

## Blokové schéma

```mermaid
flowchart LR
PC ==> STM8[STM8]
BTN <--> STM8
STM8 --> Sedmisegmenty
STM8 --> Tranzistory --> Sedmisegmenty
```

## Tabulka součástek

|  Typ součástky  |      Počet kusů      |  Cena/1 ks  | Cena  |
|:---------------:|:--------------------:|:-----------:|:-----:|
| Tlačítko        |    2                 | 2,50 Kč     | 5  Kč |
| Rezistor        |    16                | 2 Kč        | 32 Kč |
| Tranzistor      |    4                 | 2,50 Kč     | 10 Kč |
| STM8 Nucleo     |    1                 | 250 Kč      | 250 Kč|
| Sedmisegment    |    4                 | 10 Kč       | 40 Kč |

## Schéma v KiCadu

![](stopky_fotka_kicad.png)

- [PDF](stopky_kicad.pdf)

## Zapojení v realitě

![](stopky_fotka_1.jpg)
![](stopky_fotka_2.jpg)

## Zdrojový kód

- [MAIN](src/main.c)

## Závěr
Tento projekt pro mě byla relativně velká výzva. Při práci jsem narazil na spoustu mých nedostatků. S Cčkem jsem si ze začátku moc nevěděl rady, proto mi nezbývalo nic jiného než si sehnat nějaké doučování. Ze začátku mi dělalo největší problém vůbec vymyslet jak na samotný program. Hardwarové zapojení pro mě nebylo nic složitého. Po několika hodinových konzultací s doučujícím jsem začal Cčku trochu rozumět a tak jsme společnými silami dali program dohromady. Na programu jsme pracovali zhruba 15 hodin, ale projektu jako celku jsem věnoval cca 75 hodin. I na to kolik času jsem tomu věnoval tak mě to docela bavilo a s výsledkem jsem spokojený nad svá očekávání. 

## Autor
Michal Rella